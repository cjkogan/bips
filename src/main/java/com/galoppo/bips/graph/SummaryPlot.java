/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.graph;

import com.galoppo.bips.utils.Utils;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author tgaloppo
 */
public class SummaryPlot extends JFrame {
    int[][] binCounts;
    double[][] binBounds;
     
    public SummaryPlot(List<double[]> data, int nBins, String[] labels) {
        super("BIPS Summary Plot");
        init(data.toArray(new double[0][]), nBins, labels);
    }    
    
    public SummaryPlot(double[][] data, int nBins, String[] labels) {
        super("BIPS Summary Plot");
        init(data, nBins, labels);
    }
    
    private void init(double[][] data, int nBins, String[] labels){
        setPreferredSize(new Dimension(800, 800));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        computeBins(data, nBins);
        JPanel hists = new JPanel();
        hists.setLayout(new GridLayout(binCounts.length, 1));
        for (int j=0; j<binCounts.length; j++) {
            hists.add(new HistPanel(binCounts[j], binBounds[j], labels[j]));
        }        
        JScrollPane scrollPane = new JScrollPane(hists);
        add(scrollPane);
        pack();        
    }
    
    private void computeBins(double[][] data, int nBins) {
        int k = data[0].length;
        binCounts = new int[k][];
        binBounds = new double[k][];
        for (int j=0; j<k; j++) {
            double[] values = Utils.getColumnValues(data, j);
            double[] minMax = getMinMax(values);
            double min = minMax[0];
            double max = minMax[1];
            double range = max - min;
            double binSize = range / nBins;
            binBounds[j] = new double[nBins + 1];
            for (int i=0; i<nBins+1; i++) {
                binBounds[j][i] = min + i * binSize;
            }
            binCounts[j] = new int[nBins];
            for (double u : values) {
                int bin = Math.min((int)((u - min) / binSize), nBins-1);
                binCounts[j][bin] += 1;
            }
        }
    }
    
    private double[] getMinMax(double[] x) {
        double min = Double.POSITIVE_INFINITY;
        double max = Double.NEGATIVE_INFINITY;
        for (double u : x) {
            if (u < min) min = u;
            if (u > max) max = u;
        }
        return new double[] { min, max };
    }
}
