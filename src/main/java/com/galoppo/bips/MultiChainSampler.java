/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips;

import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Clark
 */
public class MultiChainSampler {
    private SamplerThread[] samplers;
    private Integer nchains;
    private int nsamples;
    private final int npar;
    private final Density[] f;
    private double[][] x0 = null;   
    
    public MultiChainSampler(Density[] f) {
        this.f = f;
        npar = f.length;
    } 

    public MultiChainSampler setNumberOfSamples(int nsamples) {
        this.nsamples = nsamples;
        for(int i=0; i<samplers.length; i++){
            samplers[i].setSamplesLength(nsamples);
        }        
        return this;
    }    
    /**
     * Sets the number of independent Markov chains to run 
     * Default is 1.
     * 
     * @param nchains Number of chains
     * @return 
     */
    public MultiChainSampler setNumberOfChains(int nchains) {
        this.nchains = nchains;
        samplers = new SamplerThread[nchains];
        if (null == x0) {
            for(int i=0; i<nchains; i++){
                samplers[i] = new SamplerThread(f);
            }
        } else {
            for(int i=0; i<nchains; i++){
                samplers[i] = new SamplerThread(f, x0[i]);
            }            
        }
        return this;
    }    
    
/**
     * Sets the number of samples to be discarded to reduce initialization
     * artifacts.  Default is 0.
     * 
     * @param nburn Number of samples to discard
     * @return 
     */
    public MultiChainSampler setBurnLength(int nburn) {
        for(int i=0; i < samplers.length; i++){
            samplers[i].setBurnLength(nburn);
        }
        return this;
    }
    
    /**
     * Sets the thinning factor used to reduce serial correlation. Only samples
     * thinStep apart will be retained in final sample set. Default is 1.
     * 
     * @param thinStep The thinning factor to be applied
     * @return 
     */
    public MultiChainSampler setThinningFactor(int thinStep) {
        for(int i=0; i<samplers.length; i++){
            samplers[i].setThinningFactor(thinStep);
        }
        return this;
    }
    
    /**
     * Set the number of samples between step-size adaptations.
     * Default is 1000.
     * .
     * @param nadapt Number of samples between adaptations
     * @return 
     */
    public MultiChainSampler setAdaptPeriod(int nadapt) {
        for(int i=0; i<samplers.length; i++){
            samplers[i].setAdaptPeriod(nadapt);
        }
        return this;
    }
    
    /**
     * Set the target acceptance percentage for adaptation.
     * Default is 0.5
     * 
     * @param adaptPct The target acceptance percentage 
     * @return  
     */
    public MultiChainSampler setAdaptPercentage(double adaptPct) {
        for(int i=0; i<samplers.length; i++){
            samplers[i].setAdaptPercentage(adaptPct);
        }
        return this;
    }   
    
    /**
     * If you are going to set the initial conditions explicitly, this 
     * must be called before setNumberOfChains()
     * 
     * @param x0
     * @return 
     */
    public MultiChainSampler setInitialConditions(double[][] x0){
        this.x0 = x0;
        return this;
    }
    
    public List<List<double[]>> sample() throws InterruptedException{
        if(null == nchains){
            this.setNumberOfChains(1);
        }
        int cores = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(cores);
        for(int i=0; i<samplers.length; i++){
            executor.execute(samplers[i]);
        }  
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        List<List<double[]>> samples = new LinkedList<>();
        for(int i=0; i<samplers.length; i++){
            samples.add(samplers[i].getSamples());
        }
        return samples;
    }
}
