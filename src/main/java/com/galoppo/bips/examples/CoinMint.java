/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import static com.galoppo.bips.Densities.*;
import com.galoppo.bips.Density;
import com.galoppo.bips.Sampler;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import com.galoppo.bips.utils.Utils;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Given several coins from a single mint, we want to explore whether the
 * mint produces "fair" coins.
 * 
 * This example estimates the distributions of the parameters (alpha, beta) of a 
 * Beta distribution governing the bias of Bernoulli distributions, given
 * samples from the Bernoulli distributions.
 * 
 * @author tgaloppo
 */
public class CoinMint {
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Please provide an input file");
            return;
        }
        
        // read the data, format is <id, y>
        // where "id" is coin id, and "y" is 0/1 for heads/tails
        double[][] data = Utils.readData(new File(args[0]), true);   
        
        // convertToCounts() counts the flips and number of heads for each
        // coin; in the process, it also counts the number of coins in the
        // input file.
        int[][] counts = convertToCounts(data);
        int n = counts.length;
        
        // We have two options for the data likelihood; we can consider each
        // flip as a draw from a Bernoulli process, as such:
    //    Density data_likelihood = (x) -> Stream.iterate(0, (j)->j+1)
    //                                      .limit(data.length)
    //                                      .mapToDouble(j -> bernoulli((int)data[j][1], x[(int)data[j][0]]))
    //                                      .sum();
    
        // Or we can consider the "heads" count for each coin as a draw from a
        // Binomial(n) process where n is the number of flips, as such:
        // (Note that this is much more efficient for the sampler)
        Density data_likelihood = (x) -> Stream.iterate(0, (j)->j+1)
                                               .limit(n)
                                               .mapToDouble(j -> binomial(counts[j][1], counts[j][0], x[j]))
                                               .sum();
               
        // We also have a likelihood for the hyperparameters, give the
        // probability values for each coin
        Density hyper_likelihood = (x) -> Arrays.stream(beta(Arrays.stream(x, 0, n).toArray(), x[n], x[n+1])).sum();
  
        // For each coin, we create a conditional density for the probability of "heads"
        Density[] model = new Density[n+2];
        String[] labels = new String[n+2];
        for (int j=0; j<n; j++) {
            final int k = j;
            model[j] = (x) -> beta(x[k], x[n], x[n+1]) + data_likelihood.p(x);
            labels[j] = String.format("p[%d]", j);
        }
        // And the conditional densities on the hyperparameters
        // (with pretty uninformative priors)
        model[n] = (x) -> uniform(x[n], 0, 25) + hyper_likelihood.p(x);
        model[n+1] = (x) -> uniform(x[n+1], 0, 25) + hyper_likelihood.p(x);
        labels[n] = "alpha"; labels[n+1] = "beta";

        // draw some samples
        List<double[]> samps = new Sampler(model)
                               .setBurnLength(20000)
                               .setThinningFactor(10)
                               .setProgressCallback((x,N) -> {if (x % 1000 == 0) System.out.printf("%d / %d\r", x, N);})
                               .sample(100000);

        System.out.println(new Summary(samps, labels).toString());
        new SummaryPlot(samps, 15, labels).setVisible(true); 
    }
    
    private static int[][] convertToCounts(double[][] flipData) {
        Map<Integer, int[]> coinFlips = new HashMap<>();
        for (double[] row : flipData) {
            int[] counts = coinFlips.get((int)row[0]);
            if (null == counts) {
                counts = new int[2];
                coinFlips.put((int)row[0], counts);
            }
            counts[0] += 1;
            counts[1] += (int)row[1];
        }
        
        int n = coinFlips.size();
        
        int[][] counts = new int[n][];
        coinFlips.entrySet().stream().forEach((entry) -> {
            counts[entry.getKey()] = entry.getValue();
        });  
        
        return counts;
    }
}
