/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

import static com.galoppo.bips.Densities.*;
import com.galoppo.bips.Density;
import com.galoppo.bips.Sampler;
import com.galoppo.bips.graph.SummaryPlot;
import com.galoppo.bips.utils.Summary;
import java.util.Arrays;
import java.util.List;

/**
 * This class serves as a short introduction to using the BIPS sampler;
 * it is not intended as an introduction to Bayesian data analysis.
 * 
 * @author tgaloppo
 */
public class Tutorial {
    public static void main(String[] args) {        
        // BIPS generates samples from a probability distribution specified
        // in terms of the conditional probability distribution of each
        // component random variable. The set of conditional probabilities is
        // supplied to the sampler as an array of Density functions.  For
        // instance, if we want to sample from a standard normal (Gaussian)
        // distribution, we construct our array of conditional probability
        // functions [trivially] as:
        Density[] densities = { 
            (x) -> gaussian(x[0], 0.0, 1.0) // x[0] ~ N(0,1)
        };
        
        // Internally, BIPS keeps track of one random variable for each
        // entry in the array of densities.  The parameter "x" passed to
        // each Density function is an array of current values for each
        // corresponding random variable.  In the above example, we indicated 
        // to BIPS that the first variable, x[0], is distributed normally
        // with mean 0 and standard deviation 1. (See the "Densities" class
        // for the list of supplied density functions).  If our joint
        // probability distribution were 2-dimensional, composed of 2
        // independent normally distributed random variables, we could
        // define the set of conditional Densities as:
        densities = new Density[] {
            (x) -> gaussian(x[0], 0.0, 1.0), // x[0] -> N(0,1)
            (x) -> gaussian(x[1], 0.0, 1.0)  // x[1] -> N(0,1)
        };
        
        // To draw some samples from this distribution, we create a Sampler
        // object and request a number of samples:
        List<double[]> samples = new Sampler(densities).sample(10000);
        
        // In this case, the samples array is now a 10000x2
        // matrix of samples... we can get a summary of the distributions
        // with the Summary class:
        System.out.println(new Summary(samples));
        
        // We can supply more meaningful labels for the variables:
        System.out.println(new Summary(samples, new String[]{ "x", "y" }));
        
        // We can also get histograms of the distribution of each
        // parameter; we supply the number of bins in the histogram and
        // the labels for the variables:
        new SummaryPlot(samples, 10, new String[]{ "x", "y" }).setVisible(true);
        
        // In MCMC sampling, some of the initial samples often do not 
        // represent the distribution well, and should be discarded.  This
        // is typically referred to as a "burn in" period, and you can tell
        // BIPS to throw away some number of samples from the beginning:
        samples = new Sampler(densities).setBurnLength(1000).sample(10000);
        
        // The result is now a 9000x2 matrix of samples... we can again
        // get the summary:
        System.out.println(new Summary(samples, new String[]{ "x", "y" }));

        // Another artifact of MCMC sampling is that consecutive samples
        // are not independent; generating independent samples is often
        // accomplished by subsampling the samples.  A simple form of 
        // such subsampling, called "thinning", keeps only every n-th
        // generated sample.  BIPS can be told to thin the samples at
        // generation time:
        samples = new Sampler(densities)
                        .setBurnLength(1000)
                        .setThinningFactor(10)
                        .sample(10000);
        
        // The result is now a 900x2 matrix of samples...
        System.out.println(new Summary(samples, new String[]{ "x", "y" }));
        
        
        // In Bayesian analyses, we will need to sample from the likelihood
        // function, given some data; lets say we have some readings from
        // a Gaussian distribution [from N(3.5, 1)]:
        double[] values = {5.651125, 2.334999, 2.670664, 2.945829, 3.309375, 
                           4.411284, 2.388855, 3.013941, 4.730799, 4.188076};
        
        // the likelihood is then product of the probabilities of each value
        // for a given mean; for numerical stability, the density functions
        // compute log density, and thus the log-likelihood is the sum
        // of the log-densities:
        Density likelihood = (x) -> Arrays.stream(gaussian(values, x[0], 1.0)).sum(); // values[i] ~ N(x[0], 1.0)
        
        // Here we have assumed that the standard deviation is 1, and specified
        // the mean as the only variable. We can sample from this distribution
        // directly:
        samples = new Sampler(new Density[] { likelihood })
                        .setBurnLength(1000)
                        .setThinningFactor(10)
                        .sample(10000);
        
        System.out.println(new Summary(samples, new String[]{ "mean" }));
        new SummaryPlot(samples, 10, new String[]{ "mean" }).setVisible(true);
        
        // We would expect the mean of the samples to match the maximum
        // likelihood estimate of the population mean; in this case,
        // that is simply the arithmetic average:
        double mean = Arrays.stream(values).average().getAsDouble();
        System.out.printf("arithmetic average = %f\n\n", mean);
        
        // From the histogram, we can see the relative likelihood of
        // various values of the mean... it should not be surprising that
        // the histogram peaks at the maximum likelihood estimate of the 
        // mean. In a Bayesian analysis, we may have some prior knowledge 
        // of the mean. In this case, we need to combine our prior knowledge
        // with the likelihood function to compute a posterior probability.
        // Perhaps we know the mean is pretty close to 3; the posterior
        // is the product of the prior probability and the likelihood...
        // again, working with log-densities, we simply add:
        Density posterior = (x) -> gaussian(x[0], 3, 0.3) + likelihood.p(x);
        
        // NOTE that the posterior we have defined here is actually 
        // only proportional to the true posterior, and hence is not a
        // probability density, since we are not computing the normalizing 
        // marginal probability of the data. This is one of the conveniences
        // of using sampling! This proportional function is sufficient
        // for obtaining samples from the true posterior:
        samples = new Sampler(new Density[] { posterior })
                        .setBurnLength(1000)
                        .setThinningFactor(10)
                        .sample(10000);
        
        System.out.println(new Summary(samples, new String[]{ "mean" }));
        new SummaryPlot(samples, 10, new String[]{ "mean" }).setVisible(true);        
        
        // Notice how this has moved the distribution... the prior is
        // influencing the location of the distribution.  If we add more
        // samples, the distribution should move toward the true mean:
        double[] values2 = {5.651125, 2.334999, 2.670664, 2.945829, 3.309375, 
                            4.411284, 2.388855, 3.013941, 4.730799, 4.188076,
                            3.155346, 1.008974, 5.258775, 3.364001, 4.459276,
                            4.094091, 5.878746, 3.381784, 2.645647, 3.608590};

        Density likelihood2 = (x) -> Arrays.stream(gaussian(values2, x[0], 1.0)).sum(); // values2[i] ~ N(x[0], 1.0)
        Density posterior2 = (x) -> gaussian(x[0], 3, 0.3) + likelihood2.p(x);          // notice prior: x[0] ~ N(3, 0.3)
        
        samples = new Sampler(new Density[] { posterior2 })
                        .setBurnLength(1000)
                        .setThinningFactor(10)
                        .sample(10000);
        
        System.out.println(new Summary(samples, new String[]{ "mean" }));
        new SummaryPlot(samples, 10, new String[]{ "mean" }).setVisible(true);              
        
        // Now one more example... again, we have some data which we
        // know comes from a Gaussian source:
        //   (data is from N(-1.0, 1.75) source)
        double[] data = {
            -0.961737723, -0.590191230, -1.249371569, -0.002153284, -2.094636076,
            2.364009772, -3.623869814, -3.222937670, -3.624617620, -0.612087960,
            -1.999731544,  1.629378372, -1.474808033, 1.507910289, -0.703267737            
        };
        
        // this time we want to infer both the mean and standard deviation;
        // we impose some priors:
        Density mu_prior = (x) -> gaussian(x[0], 0.0, 1.0); // mu ~ N(0, 1)
        Density sigma_prior = (x) -> gamma(x[1], 2.0, 0.5); // sigma ~ gamma(2, 0.5)
        
        // Define the likelihood function
        Density likelihood_data = (x) -> Arrays.stream(gaussian(data, x[0], x[1])).sum(); // data[i] ~ N(x[0], x[1])
        
        // Build array of conditional probabilties
        Density[] model = {
            (x) -> mu_prior.p(x) + likelihood_data.p(x),    // P(mu | data, sigma)
            (x) -> sigma_prior.p(x) + likelihood_data.p(x)  // P(sigma | data, mu)
        };
        
        // take samples
        samples = new Sampler(model)
                        .setBurnLength(1000)
                        .setThinningFactor(10)
                        .sample(10000);
        
        // examine results
        System.out.println(new Summary(samples, new String[]{ "mu", "sigma" }));
        new SummaryPlot(samples, 10, new String[]{ "mu", "sigma" }).setVisible(true);      
        
        // Explore the examples for more!
    }
}
