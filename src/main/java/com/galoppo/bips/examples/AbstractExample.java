/*
 * Copyright © 2016, Travis Galoppo <tjg2107@columbia.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any 
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF 
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package com.galoppo.bips.examples;

/**
 * This is a convenience class to provide a uniform way to provide
 * chain length, burn-in period, and thinning factor to the examples.
 * 
 * These are set via command line, such as:
 * $ java -Dbips.nsamples=100000 -cp BIPS.jar com.galoppo.bips.examples.<example> ...
 * 
 * @author tgaloppo
 */
public abstract class AbstractExample {   
    public static int getSampleCount() {
        return Integer.parseInt(System.getProperties().getProperty("bips.nsamples", "50000"));
    } 
    
    public static int getBurnCount() {
        return Integer.parseInt(System.getProperties().getProperty("bips.nburn", "10000"));
    }
    
    public static int getThinStep() {
        return Integer.parseInt(System.getProperties().getProperty("bips.thinStep", "4"));
    }
}
