# BIPS - Bayesian Inference Pedagogical Sampler #

## About ##

This project is composed of a lightweight Gibbs sampler and set of Bayesian inference examples aimed to:

* Demystify the inner workings of the sampling process
* Promote critical thinking about how Bayesian hierarchical models are constructed
* Serve as a learning tool for those who learn by playing with code

This project is expressly *not intended* to replace more mature tools such as JAGS or BUGS.

## Getting Started ##

* The project requires Java 8 (due to the heavy use of lambda expressions).
* The code is structured as a Maven project; you should be able to open with your favorite IDE.  

Once compiled, you can run the examples as, for example:

```
java -cp target/BIPS-1.0-SNAPSHOT.jar com.galoppo.bips.examples.GaussianParameters data/gaussian.csv
```

Check out the tutorial (located in the examples) for a quick overview of how to use the sampler.

## Contributing ##

Have a suggestion for making the code more clear? Or a cool example showing how to employ Bayesian methods? Feel free to create a pull request!

## License ##

This code is provided under the [very permissive] ISC license (https://opensource.org/licenses/ISC).

### Creator / Maintainer ###

* Travis Galoppo (tjg2107 AT columbia.edu)