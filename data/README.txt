Example data files:

gaussian.csv   - Random numbers (8) generated from the distribution N(10, 2.5)

regression.csv - Samples (30) from linear model y = 5 * x_0 - 2 * x_1 + 1.5 * x_2 + N(0, 0.5)
									(data file has columns y, x_0, x_1, x_2)

gmm.csv        - Random samples (40)drawn from a mixture of 2-dimensional Gaussians:
										20 samples from N([-4 -4], [1 0.6; 0.6 1]), and
										20 samples from N([4 4], [1 -0.8, -0.8, 1])

coinmint.csv   - Coin flip data for several coins from the same mint process.  Generated via R
									script (misc/CoinMint.R)
